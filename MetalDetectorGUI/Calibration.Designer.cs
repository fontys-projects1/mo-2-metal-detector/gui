﻿namespace MetalDetectorGUI
{
    partial class Calibration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comPort_label = new System.Windows.Forms.Label();
            this.setToZero_button = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.threshold_box = new System.Windows.Forms.NumericUpDown();
            this.setThreshold_button = new System.Windows.Forms.Button();
            this.setMax_button = new System.Windows.Forms.Button();
            this.setCurrentValue_checkBox = new System.Windows.Forms.CheckBox();
            this.preDefValue_box = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.selected_groepBox = new System.Windows.Forms.ListBox();
            this.detector_box = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.xOffset_button = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.addTuning_button = new System.Windows.Forms.Button();
            this.tuning_numericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.threshold_box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preDefValue_box)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tuning_numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // comPort_label
            // 
            this.comPort_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comPort_label.AutoSize = true;
            this.comPort_label.Location = new System.Drawing.Point(9, 23);
            this.comPort_label.Name = "comPort_label";
            this.comPort_label.Size = new System.Drawing.Size(27, 13);
            this.comPort_label.TabIndex = 0;
            this.comPort_label.Text = "com";
            // 
            // setToZero_button
            // 
            this.setToZero_button.Location = new System.Drawing.Point(6, 48);
            this.setToZero_button.Name = "setToZero_button";
            this.setToZero_button.Size = new System.Drawing.Size(98, 23);
            this.setToZero_button.TabIndex = 2;
            this.setToZero_button.Text = "Set Min value";
            this.setToZero_button.UseVisualStyleBackColor = true;
            this.setToZero_button.Click += new System.EventHandler(this.setToZero_button_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // threshold_box
            // 
            this.threshold_box.CausesValidation = false;
            this.threshold_box.Location = new System.Drawing.Point(10, 21);
            this.threshold_box.Margin = new System.Windows.Forms.Padding(1);
            this.threshold_box.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
            this.threshold_box.Name = "threshold_box";
            this.threshold_box.Size = new System.Drawing.Size(68, 20);
            this.threshold_box.TabIndex = 4;
            this.threshold_box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.threshold_box_KeyPress);
            // 
            // setThreshold_button
            // 
            this.setThreshold_button.Location = new System.Drawing.Point(82, 20);
            this.setThreshold_button.Name = "setThreshold_button";
            this.setThreshold_button.Size = new System.Drawing.Size(98, 23);
            this.setThreshold_button.TabIndex = 6;
            this.setThreshold_button.Text = "Set threshold";
            this.setThreshold_button.UseVisualStyleBackColor = true;
            this.setThreshold_button.Click += new System.EventHandler(this.setThreshold_button_Click);
            // 
            // setMax_button
            // 
            this.setMax_button.Location = new System.Drawing.Point(5, 19);
            this.setMax_button.Name = "setMax_button";
            this.setMax_button.Size = new System.Drawing.Size(98, 23);
            this.setMax_button.TabIndex = 6;
            this.setMax_button.Text = "Set Max value";
            this.setMax_button.UseVisualStyleBackColor = true;
            this.setMax_button.Click += new System.EventHandler(this.setMax_button_Click);
            // 
            // setCurrentValue_checkBox
            // 
            this.setCurrentValue_checkBox.AutoSize = true;
            this.setCurrentValue_checkBox.BackColor = System.Drawing.Color.Transparent;
            this.setCurrentValue_checkBox.Checked = true;
            this.setCurrentValue_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.setCurrentValue_checkBox.Location = new System.Drawing.Point(5, 89);
            this.setCurrentValue_checkBox.Name = "setCurrentValue_checkBox";
            this.setCurrentValue_checkBox.Size = new System.Drawing.Size(107, 17);
            this.setCurrentValue_checkBox.TabIndex = 7;
            this.setCurrentValue_checkBox.Text = "Set current value";
            this.setCurrentValue_checkBox.UseVisualStyleBackColor = false;
            this.setCurrentValue_checkBox.CheckedChanged += new System.EventHandler(this.setCurrentValue_checkBox_CheckedChanged);
            // 
            // preDefValue_box
            // 
            this.preDefValue_box.CausesValidation = false;
            this.preDefValue_box.Enabled = false;
            this.preDefValue_box.Location = new System.Drawing.Point(5, 110);
            this.preDefValue_box.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
            this.preDefValue_box.Name = "preDefValue_box";
            this.preDefValue_box.Size = new System.Drawing.Size(98, 20);
            this.preDefValue_box.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.setThreshold_button);
            this.groupBox1.Controls.Add(this.threshold_box);
            this.groupBox1.Location = new System.Drawing.Point(12, 359);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(189, 51);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Threshold";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.preDefValue_box);
            this.groupBox2.Controls.Add(this.setToZero_button);
            this.groupBox2.Controls.Add(this.setMax_button);
            this.groupBox2.Controls.Add(this.setCurrentValue_checkBox);
            this.groupBox2.Location = new System.Drawing.Point(378, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(112, 141);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Define values";
            // 
            // selected_groepBox
            // 
            this.selected_groepBox.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.selected_groepBox.FormattingEnabled = true;
            this.selected_groepBox.Location = new System.Drawing.Point(8, 19);
            this.selected_groepBox.Name = "selected_groepBox";
            this.selected_groepBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.selected_groepBox.Size = new System.Drawing.Size(98, 108);
            this.selected_groepBox.TabIndex = 11;
            // 
            // detector_box
            // 
            this.detector_box.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.detector_box.Location = new System.Drawing.Point(12, 12);
            this.detector_box.Name = "detector_box";
            this.detector_box.Size = new System.Drawing.Size(360, 341);
            this.detector_box.TabIndex = 11;
            this.detector_box.TabStop = false;
            this.detector_box.Text = "Detectors";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.comPort_label);
            this.groupBox3.Location = new System.Drawing.Point(378, 359);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(112, 51);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Connection";
            // 
            // xOffset_button
            // 
            this.xOffset_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.xOffset_button.Location = new System.Drawing.Point(8, 19);
            this.xOffset_button.Name = "xOffset_button";
            this.xOffset_button.Size = new System.Drawing.Size(98, 23);
            this.xOffset_button.TabIndex = 5;
            this.xOffset_button.Text = "Send X offset";
            this.xOffset_button.UseVisualStyleBackColor = true;
            this.xOffset_button.Click += new System.EventHandler(this.xOffset_button_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.xOffset_button);
            this.groupBox4.Location = new System.Drawing.Point(378, 159);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(112, 51);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "X position";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.selected_groepBox);
            this.groupBox5.Location = new System.Drawing.Point(378, 216);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(112, 137);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Selected";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox6.Controls.Add(this.addTuning_button);
            this.groupBox6.Controls.Add(this.tuning_numericUpDown);
            this.groupBox6.Location = new System.Drawing.Point(207, 359);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(165, 51);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Tune the reading";
            // 
            // addTuning_button
            // 
            this.addTuning_button.Location = new System.Drawing.Point(82, 20);
            this.addTuning_button.Name = "addTuning_button";
            this.addTuning_button.Size = new System.Drawing.Size(77, 23);
            this.addTuning_button.TabIndex = 6;
            this.addTuning_button.Text = "Add mm";
            this.addTuning_button.UseVisualStyleBackColor = true;
            this.addTuning_button.Click += new System.EventHandler(this.addTuning_button_Click);
            // 
            // tuning_numericUpDown
            // 
            this.tuning_numericUpDown.CausesValidation = false;
            this.tuning_numericUpDown.Location = new System.Drawing.Point(10, 21);
            this.tuning_numericUpDown.Margin = new System.Windows.Forms.Padding(1);
            this.tuning_numericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.tuning_numericUpDown.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.tuning_numericUpDown.Name = "tuning_numericUpDown";
            this.tuning_numericUpDown.Size = new System.Drawing.Size(68, 20);
            this.tuning_numericUpDown.TabIndex = 4;
            this.tuning_numericUpDown.Tag = "";
            this.tuning_numericUpDown.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tuning_numericUpDown_KeyPress);
            // 
            // Calibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 417);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.detector_box);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Calibration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calibration";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Calibration_FormClosed);
            this.Load += new System.EventHandler(this.Calibration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.threshold_box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preDefValue_box)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tuning_numericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label comPort_label;
        private System.Windows.Forms.Button setToZero_button;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NumericUpDown threshold_box;
        private System.Windows.Forms.Button setThreshold_button;
        private System.Windows.Forms.Button setMax_button;
        private System.Windows.Forms.CheckBox setCurrentValue_checkBox;
        private System.Windows.Forms.NumericUpDown preDefValue_box;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox selected_groepBox;
        private System.Windows.Forms.GroupBox detector_box;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button xOffset_button;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button addTuning_button;
        private System.Windows.Forms.NumericUpDown tuning_numericUpDown;
    }
}