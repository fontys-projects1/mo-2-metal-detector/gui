﻿namespace MetalDetectorGUI
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comPort_box = new System.Windows.Forms.ComboBox();
            this.connectComPort_button = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.comStatus_label = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comStatus_timer = new System.Windows.Forms.Timer(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.speed_label = new System.Windows.Forms.Label();
            this.length_average_label = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.time_label = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.widthMin_label = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.openCalibration = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.statusTImer = new System.Windows.Forms.Timer(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.openHistory_button = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.showLiveInfo_checkbox = new System.Windows.Forms.CheckBox();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.object_groupBox = new System.Windows.Forms.GroupBox();
            this.widthMax_label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.state_label = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.length_1_label = new System.Windows.Forms.Label();
            this.length_2_label = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.detectorInfo_panel = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.object_groupBox.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // comPort_box
            // 
            this.comPort_box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comPort_box.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPort_box.FormattingEnabled = true;
            this.comPort_box.Location = new System.Drawing.Point(735, 22);
            this.comPort_box.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comPort_box.Name = "comPort_box";
            this.comPort_box.Size = new System.Drawing.Size(219, 37);
            this.comPort_box.TabIndex = 0;
            this.comPort_box.SelectedIndexChanged += new System.EventHandler(this.comPort_box_SelectedIndexChanged);
            this.comPort_box.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comPort_box_MouseClick);
            // 
            // connectComPort_button
            // 
            this.connectComPort_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.connectComPort_button.Location = new System.Drawing.Point(968, 22);
            this.connectComPort_button.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.connectComPort_button.Name = "connectComPort_button";
            this.connectComPort_button.Size = new System.Drawing.Size(175, 47);
            this.connectComPort_button.TabIndex = 1;
            this.connectComPort_button.Text = "Connect";
            this.connectComPort_button.UseVisualStyleBackColor = true;
            this.connectComPort_button.Click += new System.EventHandler(this.connectComPort_button_Click);
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 115200;
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(28, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "Com status:";
            // 
            // comStatus_label
            // 
            this.comStatus_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comStatus_label.AutoSize = true;
            this.comStatus_label.Location = new System.Drawing.Point(166, 31);
            this.comStatus_label.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.comStatus_label.Name = "comStatus_label";
            this.comStatus_label.Size = new System.Drawing.Size(132, 29);
            this.comStatus_label.TabIndex = 3;
            this.comStatus_label.Text = "Disconnect";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.connectComPort_button);
            this.panel1.Controls.Add(this.comStatus_label);
            this.panel1.Controls.Add(this.comPort_box);
            this.panel1.Location = new System.Drawing.Point(0, 638);
            this.panel1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1171, 96);
            this.panel1.TabIndex = 4;
            // 
            // comStatus_timer
            // 
            this.comStatus_timer.Enabled = true;
            this.comStatus_timer.Interval = 500;
            this.comStatus_timer.Tick += new System.EventHandler(this.comStatus_timer_Tick);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(170, 573);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 29);
            this.label5.TabIndex = 11;
            this.label5.Text = "Speed:";
            // 
            // speed_label
            // 
            this.speed_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.speed_label.AutoSize = true;
            this.speed_label.Location = new System.Drawing.Point(266, 573);
            this.speed_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.speed_label.Name = "speed_label";
            this.speed_label.Size = new System.Drawing.Size(72, 29);
            this.speed_label.TabIndex = 13;
            this.speed_label.Text = "mm/s";
            // 
            // length_average_label
            // 
            this.length_average_label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.length_average_label.AutoSize = true;
            this.length_average_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.length_average_label.Location = new System.Drawing.Point(236, 76);
            this.length_average_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.length_average_label.Name = "length_average_label";
            this.length_average_label.Size = new System.Drawing.Size(107, 40);
            this.length_average_label.TabIndex = 15;
            this.length_average_label.Text = "0 mm";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(89, 76);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 40);
            this.label7.TabIndex = 14;
            this.label7.Text = "Length:";
            // 
            // time_label
            // 
            this.time_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.time_label.AutoSize = true;
            this.time_label.Location = new System.Drawing.Point(266, 602);
            this.time_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.time_label.Name = "time_label";
            this.time_label.Size = new System.Drawing.Size(45, 29);
            this.time_label.TabIndex = 17;
            this.time_label.Text = "ms";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 602);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(254, 29);
            this.label8.TabIndex = 16;
            this.label8.Text = "Time of measurement:";
            // 
            // widthMin_label
            // 
            this.widthMin_label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.widthMin_label.AutoSize = true;
            this.widthMin_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.widthMin_label.Location = new System.Drawing.Point(236, 141);
            this.widthMin_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.widthMin_label.Name = "widthMin_label";
            this.widthMin_label.Size = new System.Drawing.Size(107, 40);
            this.widthMin_label.TabIndex = 21;
            this.widthMin_label.Text = "0 mm";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(42, 141);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(197, 40);
            this.label11.TabIndex = 20;
            this.label11.Text = "Width min: ";
            // 
            // openCalibration
            // 
            this.openCalibration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.openCalibration.Location = new System.Drawing.Point(436, 45);
            this.openCalibration.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.openCalibration.Name = "openCalibration";
            this.openCalibration.Size = new System.Drawing.Size(175, 51);
            this.openCalibration.TabIndex = 22;
            this.openCalibration.Text = "Calibrate";
            this.openCalibration.UseVisualStyleBackColor = true;
            this.openCalibration.Click += new System.EventHandler(this.openCalibration_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(-2, -2);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.progressBar1.MarqueeAnimationSpeed = 10;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1176, 16);
            this.progressBar1.Step = 100;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 23;
            this.progressBar1.Visible = false;
            // 
            // statusTImer
            // 
            this.statusTImer.Enabled = true;
            this.statusTImer.Interval = 500;
            this.statusTImer.Tick += new System.EventHandler(this.statusTimer_Tick);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 23);
            this.label9.TabIndex = 0;
            // 
            // openHistory_button
            // 
            this.openHistory_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.openHistory_button.Location = new System.Drawing.Point(247, 45);
            this.openHistory_button.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.openHistory_button.Name = "openHistory_button";
            this.openHistory_button.Size = new System.Drawing.Size(175, 51);
            this.openHistory_button.TabIndex = 25;
            this.openHistory_button.Text = "History";
            this.openHistory_button.UseVisualStyleBackColor = true;
            this.openHistory_button.Click += new System.EventHandler(this.openHistory_button_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.showLiveInfo_checkbox);
            this.groupBox2.Controls.Add(this.openHistory_button);
            this.groupBox2.Controls.Add(this.openCalibration);
            this.groupBox2.Location = new System.Drawing.Point(497, 506);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.groupBox2.Size = new System.Drawing.Size(646, 118);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // showLiveInfo_checkbox
            // 
            this.showLiveInfo_checkbox.AutoSize = true;
            this.showLiveInfo_checkbox.Location = new System.Drawing.Point(33, 54);
            this.showLiveInfo_checkbox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.showLiveInfo_checkbox.Name = "showLiveInfo_checkbox";
            this.showLiveInfo_checkbox.Size = new System.Drawing.Size(194, 33);
            this.showLiveInfo_checkbox.TabIndex = 26;
            this.showLiveInfo_checkbox.Text = "Show live info";
            this.showLiveInfo_checkbox.UseVisualStyleBackColor = true;
            this.showLiveInfo_checkbox.CheckedChanged += new System.EventHandler(this.showLiveInfo_checkbox_CheckedChanged);
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(0, 0);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(100, 23);
            this.progressBar2.TabIndex = 0;
            // 
            // object_groupBox
            // 
            this.object_groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.object_groupBox.Controls.Add(this.widthMax_label);
            this.object_groupBox.Controls.Add(this.label2);
            this.object_groupBox.Controls.Add(this.widthMin_label);
            this.object_groupBox.Controls.Add(this.label7);
            this.object_groupBox.Controls.Add(this.label11);
            this.object_groupBox.Controls.Add(this.length_average_label);
            this.object_groupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.object_groupBox.Location = new System.Drawing.Point(226, 136);
            this.object_groupBox.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.object_groupBox.Name = "object_groupBox";
            this.object_groupBox.Padding = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.object_groupBox.Size = new System.Drawing.Size(448, 286);
            this.object_groupBox.TabIndex = 27;
            this.object_groupBox.TabStop = false;
            this.object_groupBox.Text = "Object details";
            // 
            // widthMax_label
            // 
            this.widthMax_label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.widthMax_label.AutoSize = true;
            this.widthMax_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.widthMax_label.Location = new System.Drawing.Point(236, 205);
            this.widthMax_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.widthMax_label.Name = "widthMax_label";
            this.widthMax_label.Size = new System.Drawing.Size(107, 40);
            this.widthMax_label.TabIndex = 22;
            this.widthMax_label.Text = "0 mm";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 205);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 40);
            this.label2.TabIndex = 23;
            this.label2.Text = "Width max: ";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.Controls.Add(this.state_label);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.length_1_label);
            this.panel3.Controls.Add(this.length_2_label);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Location = new System.Drawing.Point(79, 448);
            this.panel3.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(320, 127);
            this.panel3.TabIndex = 21;
            // 
            // state_label
            // 
            this.state_label.AutoSize = true;
            this.state_label.Location = new System.Drawing.Point(182, 25);
            this.state_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.state_label.Name = "state_label";
            this.state_label.Size = new System.Drawing.Size(53, 29);
            this.state_label.TabIndex = 21;
            this.state_label.Text = "Idle";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(104, 25);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 29);
            this.label3.TabIndex = 20;
            this.label3.Text = "State:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(67, 83);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 29);
            this.label6.TabIndex = 19;
            this.label6.Text = "Lengte 2:";
            // 
            // length_1_label
            // 
            this.length_1_label.AutoSize = true;
            this.length_1_label.Location = new System.Drawing.Point(182, 54);
            this.length_1_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.length_1_label.Name = "length_1_label";
            this.length_1_label.Size = new System.Drawing.Size(53, 29);
            this.length_1_label.TabIndex = 12;
            this.length_1_label.Text = "mm";
            // 
            // length_2_label
            // 
            this.length_2_label.AutoSize = true;
            this.length_2_label.Location = new System.Drawing.Point(182, 83);
            this.length_2_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.length_2_label.Name = "length_2_label";
            this.length_2_label.Size = new System.Drawing.Size(53, 29);
            this.length_2_label.TabIndex = 18;
            this.length_2_label.Text = "mm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(67, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 29);
            this.label4.TabIndex = 10;
            this.label4.Text = "Lengte 1:";
            // 
            // detectorInfo_panel
            // 
            this.detectorInfo_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.detectorInfo_panel.Location = new System.Drawing.Point(807, 27);
            this.detectorInfo_panel.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.detectorInfo_panel.Name = "detectorInfo_panel";
            this.detectorInfo_panel.Size = new System.Drawing.Size(336, 466);
            this.detectorInfo_panel.TabIndex = 22;
            this.detectorInfo_panel.Visible = false;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 734);
            this.Controls.Add(this.detectorInfo_panel);
            this.Controls.Add(this.object_groupBox);
            this.Controls.Add(this.speed_label);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.time_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1048, 327);
            this.Name = "mainForm";
            this.Text = "Metal detector";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.object_groupBox.ResumeLayout(false);
            this.object_groupBox.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comPort_box;
        private System.Windows.Forms.Button connectComPort_button;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label comStatus_label;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer comStatus_timer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label speed_label;
        private System.Windows.Forms.Label length_average_label;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label time_label;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button openCalibration;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer statusTImer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button openHistory_button;
        private System.Windows.Forms.Label widthMin_label;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label widthMax_label;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label length_1_label;
        private System.Windows.Forms.Label length_2_label;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label state_label;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox showLiveInfo_checkbox;
        private System.Windows.Forms.Panel detectorInfo_panel;
        private System.Windows.Forms.GroupBox object_groupBox;
    }
}

