﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetalDetectorGUI
{
    public partial class Calibration : Form
    {
        private SerialPort _serialConnection;
        private uint[] _detectorValues;
        private Label[] _labels;
        private Label[] _thesholdLabels;
        private CheckBox[] _detectorCheckboxes;
        private NumericUpDown[] _xOffset;
        private mainForm _mainForm;
        private Int16 oldTuning = 0;

        const int LOW_THRESHOLD = 10;
        const char START_CHAR = '#';        // 0b0010 0011

        const int LABEL_SIZE = 100;
        const int DASH_SIZE = 15;
        const int CHECKBOXES_SIZE = 50;
        const int THRESHOLD_LABEL_SIZE = 100;
        const int X_MODULE_OFFSET_SIZE = 65;
        const int LABEL_OFFSET_X = 12;
        const int THRESHOLD_LABEL_OFFSET_X = LABEL_OFFSET_X + LABEL_SIZE;
        const int X_MODULE_OFFSET_OFFSET_X = THRESHOLD_LABEL_OFFSET_X + THRESHOLD_LABEL_SIZE;
        const int DASH_OFFSET_X = X_MODULE_OFFSET_OFFSET_X + X_MODULE_OFFSET_SIZE;
        const int CHECKBOXES_OFFSET_X = DASH_OFFSET_X + DASH_SIZE;
        const int LABEL_OFFSET_Y = 25;
        const int LABEL_TO_LABEL = 35;
        const int TEXT_HEIGHT = 13;
        const int TEXT_WIDTH = 35;

        enum Detectors
        {
            Detector1 = 0,
            Detector2,
            Detector3,
            Detector4,
            Detector5,
            DetectorCount
        };

        enum CommunicationSetCommands
        {
            SetThreshold = 0,
            SetZeroLevel,
            SetMaxLevel,
            SetX_offset,
            SetGlobalSettings,
            GetThreshold,
            GetX_offset,
            GetTuning,
            SetTuning
        };

        // Constructor
        public Calibration(SerialPort serialConnection, uint[] detectorValues)
        {
            InitializeComponent();
            _serialConnection = serialConnection;
            _detectorValues = detectorValues;
            _labels = new Label[_detectorValues.Length];
            _thesholdLabels = new Label[_detectorValues.Length];
            _detectorCheckboxes = new CheckBox[_detectorValues.Length];
            _xOffset = new NumericUpDown[_detectorValues.Length];
        }


        // enable the mainform
        private void Calibration_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Close the application when the form is closed
            _mainForm.Enabled = true;
            _mainForm.Focus();
        }

        // on load
        private void Calibration_Load(object sender, EventArgs e)
        {
            // import the x offset values from the main form
            _mainForm = (mainForm)Application.OpenForms["mainForm"];
            UInt16[] xOffsets = new UInt16[_detectorValues.Length];
            
            xOffsets = _mainForm.getDetectorX_offset();

            // resize the form if there are more detectors than the box can fit
            int totalTextHeight = (int)(LABEL_OFFSET_Y + (LABEL_TO_LABEL * (_detectorValues.Length - 1)) + TEXT_HEIGHT);
            int formExtraHeight = this.Height - detector_box.Height;
            if (totalTextHeight > detector_box.Height)
            {
                this.Height = totalTextHeight + formExtraHeight;
            }

            // create the labels and checkboxes for each detector
            comPort_label.Text = _serialConnection.PortName;
            //selection_CheckedChanged();
            sendData(CommunicationSetCommands.GetThreshold, Detectors.DetectorCount);

            for (int i = 0; i < _detectorValues.Length; i++)
            {
                // add labels of the detector 
                _labels[i] = new Label();
                _labels[i].AutoSize = true;
                _labels[i].Location = new System.Drawing.Point(LABEL_OFFSET_X, LABEL_OFFSET_Y + (int)i * LABEL_TO_LABEL);
                _labels[i].Name = "detector" + i;
                _labels[i].Size = new System.Drawing.Size(TEXT_WIDTH, TEXT_HEIGHT);
                _labels[i].TabIndex = i;
                _labels[i].Text = "Detector ";
                _labels[i].Font = new Font(_labels[i].Font, FontStyle.Bold);
                //this.Controls.Add(_labels[i]);
                detector_box.Controls.Add(_labels[i]);

                // add checkboxes next to the labels
                _detectorCheckboxes[i] = new CheckBox();
                _detectorCheckboxes[i].AutoSize = true;
                _detectorCheckboxes[i].Location = new System.Drawing.Point(CHECKBOXES_OFFSET_X, LABEL_OFFSET_Y + (int)i * LABEL_TO_LABEL);
                _detectorCheckboxes[i].Name = "checkBox" + i;
                _detectorCheckboxes[i].Size = new System.Drawing.Size(TEXT_WIDTH, TEXT_HEIGHT);
                _detectorCheckboxes[i].TabIndex = i;
                _detectorCheckboxes[i].Text = "Select";
                _detectorCheckboxes[i].BackColor = Color.Transparent;
                _detectorCheckboxes[i].CheckedChanged += new System.EventHandler(this.selection_CheckedChanged);
                //this.Controls.Add(_detectorCheckboxes[i]);
                detector_box.Controls.Add(_detectorCheckboxes[i]);

                // add dash
                Label tmpLabel = new Label();
                tmpLabel.AutoSize = true;
                tmpLabel.Name = "dash" + i;
                tmpLabel.Size = new System.Drawing.Size(TEXT_WIDTH, TEXT_HEIGHT);
                tmpLabel.Text = "-";
                tmpLabel.TabIndex = i;
                tmpLabel.Location = new System.Drawing.Point(DASH_OFFSET_X, LABEL_OFFSET_Y + (int)i * LABEL_TO_LABEL);
                tmpLabel.Font = new Font(tmpLabel.Font, FontStyle.Bold);
                detector_box.Controls.Add(tmpLabel);

                // add labels for the threshold
                _thesholdLabels[i] = new Label();
                _thesholdLabels[i].AutoSize = true;
                _thesholdLabels[i].Location = new System.Drawing.Point(THRESHOLD_LABEL_OFFSET_X, LABEL_OFFSET_Y + (int)i * LABEL_TO_LABEL);
                _thesholdLabels[i].Name = "threshold" + i;
                _thesholdLabels[i].Size = new System.Drawing.Size(TEXT_WIDTH, TEXT_HEIGHT);
                _thesholdLabels[i].TabIndex = i;
                _thesholdLabels[i].Text = " - Threshold: ";
                _thesholdLabels[i].Font = new Font(_thesholdLabels[i].Font, FontStyle.Regular);
                //this.Controls.Add(_labels[i]);
                detector_box.Controls.Add(_thesholdLabels[i]);

                // add numeric up down for x offset
                // skip the first one
                if (i > 0)
                {
                    _xOffset[i] = new NumericUpDown();
                    _xOffset[i].Location = new System.Drawing.Point(X_MODULE_OFFSET_OFFSET_X, LABEL_OFFSET_Y + (int)i * LABEL_TO_LABEL - 4);
                    _xOffset[i].Maximum = new decimal(new int[] {
                                1000,
                                0,
                                0,
                                0});
                    _xOffset[i].Name = "xOffset" + i;
                    _xOffset[i].Size = new System.Drawing.Size(40, 35);
                    _xOffset[i].TabIndex = i;
                    _xOffset[i].CausesValidation = false;
                    // add functon when enter is pressed
                    _xOffset[i].KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.xOffset_KeyPress);
                    detector_box.Controls.Add(_xOffset[i]);

                    // add dash
                    Label tmpLabel2 = new Label();
                    tmpLabel2.AutoSize = true;
                    tmpLabel2.Name = "xOffsetUnit" + i;
                    tmpLabel2.Size = new System.Drawing.Size(TEXT_WIDTH, TEXT_HEIGHT);
                    tmpLabel2.Text = "mm";
                    tmpLabel2.TabIndex = i;
                    tmpLabel2.Location = new System.Drawing.Point(X_MODULE_OFFSET_OFFSET_X + 40, LABEL_OFFSET_Y + (int)i * LABEL_TO_LABEL);
                    detector_box.Controls.Add(tmpLabel2);

                    // set the value
                    _xOffset[i].Value = xOffsets[i];
                }

            }

            selection_CheckedChanged(sender, e);
            timer1.Start();
        }

        private void xOffset_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                // get the index of the detector
                int index = int.Parse(((NumericUpDown)sender).Name.Substring(7));

                // ask to confirm the change
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to change the X offset of Detector " + index + " to " + ((NumericUpDown)sender).Value + " mm?", "Confirm", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    // send the command
                    sendData(CommunicationSetCommands.SetX_offset, (Detectors)index, (uint)((NumericUpDown)sender).Value, sizeof(UInt16));
                }
            }
        }


        // on timer tick
        private void timer1_Tick(object sender, EventArgs e)
        {
            
            uint[] thresholdValues = _mainForm.getDetectorThresholds();
            
            // set colors to current values
            // set the color green if the value is above the threshold
            // set the color red if the value is zero
            // set the color orange if the value is between zero and the LOW_THRESHOLD
            // else black
            for (uint i = 0; i < _detectorValues.Length; i++)
            {
                _labels[i].Text = "Detector " + i.ToString() + ": " + _detectorValues[i].ToString();
                if (_detectorValues[i] > thresholdValues[i])
                {
                    _labels[i].ForeColor = Color.Green;
                }
                else
                {

                    if (_detectorValues[i] > LOW_THRESHOLD)
                    {
                        _labels[i].ForeColor = Color.Black;
                    }
                    else if (_detectorValues[i] == 0)
                    {
                        _labels[i].ForeColor = Color.Red;
                    }
                    else
                    {
                        _labels[i].ForeColor = Color.Orange;
                    }
                }
            }

            // display the threshold values
            // get the threshold array from the main form
            for (uint i = 0; i < thresholdValues.Length; i++)
            {
                _thesholdLabels[i].Text = " - Threshold: " + thresholdValues[i].ToString();
            }


            // set the tuning value
            if (oldTuning != _mainForm.getTuningValue())
            {
                tuning_numericUpDown.Value = _mainForm.getTuningValue();
                oldTuning = _mainForm.getTuningValue();
            }
        }

        // send data to serial port
        private void sendData(CommunicationSetCommands command, Detectors detectorIndex, uint data = 0, uint dataLen = 1)
        {
            byte[] dataBuf = BitConverter.GetBytes(data);
            //dataBuf.Reverse();

            byte[] buffer = new byte[dataLen + 4];
        // add start char
            buffer[0] = (byte)START_CHAR;
            // add command
            buffer[1] = (byte)command;
            // add the data length
            buffer[2] = (byte)(dataLen);
            // add detector
            buffer[3] = (byte)detectorIndex;
            // add data
            for (int i = 0; i < dataLen; i++)
            {
                buffer[4 + i] = dataBuf[i];
            }
            _serialConnection.Write(buffer, 0, buffer.Length);
            while (_serialConnection.BytesToWrite > 0) ;
        }

        // get the selected detectors and return a bool array, if no detector is selected return null
        private bool getSelectedDetectors(bool[] selectedDetectors)
        {
            bool anySelected = false;
            for (int i = 0; i < _detectorValues.Length; i++)
            {
                selectedDetectors[i] = _detectorCheckboxes[i].Checked;
                if (selectedDetectors[i]) anySelected = true;
            }
            return anySelected;
        }


        // send the given data to the selected detectors
        // if no detector is selected, send the data to all detectors
        private void sendToSelected(CommunicationSetCommands command, UInt16 value)
        {
            // get the selected detectors
            bool[] selectedDetectors = new bool[_detectorValues.Length];
            bool anySelected = getSelectedDetectors(selectedDetectors);

            // send the data to the selected detectors
            if (anySelected)
            {
                for (int i = 0; i < _detectorValues.Length; i++)
                {
                    if (selectedDetectors[i])
                    {
                        sendData(command, (Detectors)i, value, sizeof(UInt16));
                    }
                }
            }
            else
            {
                sendData(command, Detectors.DetectorCount, value, sizeof(UInt16));
            }
        }

        // set the zero value
        private void setToZero_button_Click(object sender, EventArgs e)
        {           
            // 
            if (setCurrentValue_checkBox.Checked)
            {
                // create messagebox to ask if the user is sure
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to set the zero level to the current value?", "Set to zero", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    // set the current value as min value
                    sendToSelected(CommunicationSetCommands.SetZeroLevel, 0);
                }
                else if (dialogResult == DialogResult.No)
                {
                    // do nothing
                }
            }
            else
            {
                UInt16 value = (UInt16)preDefValue_box.Value;
                // create messagebox to ask if the user is sure
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to set the zero level to " + value.ToString() + "?", "Set to zero", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    sendToSelected(CommunicationSetCommands.SetZeroLevel, value);
                }
                else if (dialogResult == DialogResult.No)
                {
                    // do nothing
                }
            }
        }

        // set the max value
        private void setMax_button_Click(object sender, EventArgs e)
        {            
            if (setCurrentValue_checkBox.Checked)
            {
                // create messagebox to ask if the user is sure
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to set the max level to the current value?", "Set to max", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    // set the current value as max value
                    sendToSelected(CommunicationSetCommands.SetMaxLevel, 0);
                }
                else if (dialogResult == DialogResult.No)
                {
                    // do nothing
                }
            }
            else
            {
                UInt16 value = (UInt16)preDefValue_box.Value;
                // create messagebox to ask if the user is sure
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to set the max level to " + value.ToString() + "?", "Set to max", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    // set the value of the box as max value
                    sendToSelected(CommunicationSetCommands.SetMaxLevel, value);
                }
                else if (dialogResult == DialogResult.No)
                {
                    // do nothing
                }
            }
        }

        // set the threshold value
        private void setThreshold_button_Click(object sender, EventArgs e)
        {            
            // create messagebox to ask if the user is sure and display the current threshold (threshold_box) in the message
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to set the threshold to " + threshold_box.Text + "?", "Set threshold", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                sendToSelected(CommunicationSetCommands.SetThreshold, (UInt16)threshold_box.Value);
                sendData(CommunicationSetCommands.GetThreshold, Detectors.DetectorCount);
            }
            else if (dialogResult == DialogResult.No)
            {
                // do nothing
            }

        }

        // set the threshold value when enter is pressed
        private void threshold_box_KeyPress(object sender, KeyPressEventArgs e)
        {
            //when the user presses enter, the threshold is set
            if (e.KeyChar == (char)Keys.Enter)
            {
                setThreshold_button_Click(sender, e);
            }
        }

        // enable or disable the predefined value box
        private void setCurrentValue_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            preDefValue_box.Enabled = !setCurrentValue_checkBox.Checked;
        }

        // update the selected detectors list
        private void selection_CheckedChanged(object sender, EventArgs e)
        {
            // add detectors to the selected_groupBox
            selected_groepBox.Items.Clear();
            for (uint i = 0; i < _detectorValues.Length; i++)
            {
                if (_detectorCheckboxes[i].Checked)
                {
                    selected_groepBox.Items.Add("Detector " + (i + 1).ToString());
                }
            }
            
            if (selected_groepBox.Items.Count == 0)
            {
                selected_groepBox.Items.Add("Apply to all");
            }
        }

        // set the x offset
        private void xOffset_button_Click(object sender, EventArgs e)
        {
            bool[] selectedDetectors = new bool[_detectorValues.Length];
            bool anySelected = getSelectedDetectors(selectedDetectors);

            if (anySelected)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to apply the offset to selected detectors?", "Apply offset", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    for (int i = 0; i < _detectorValues.Length; i++)
                    {
                        if (selectedDetectors[i])
                        {
                            sendData(CommunicationSetCommands.SetX_offset, (Detectors)i, (UInt16)_xOffset[i].Value, sizeof(UInt16));
                        }
                    }
                }
            }
            else
            {
                // ask to continu to apply to all
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to apply the offset to all detectors?", "Apply offset", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    for (int i = 1; i < _detectorValues.Length; i++)
                    {
                        sendData(CommunicationSetCommands.SetX_offset, (Detectors)i, (UInt16)_xOffset[i].Value, sizeof(UInt16));
                    }
                }
            }
        }

        private void addTuning_button_Click(object sender, EventArgs e)
        {
            // ask to continu
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to edit the tuning?", "Edit tuning", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Int16 value = (Int16)tuning_numericUpDown.Value;
                sendData(CommunicationSetCommands.SetTuning, (Detectors)0, (UInt16)value, sizeof(UInt16));
            }
        }

        private void tuning_numericUpDown_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                addTuning_button_Click(sender, e);
            }
        }
    }
}
