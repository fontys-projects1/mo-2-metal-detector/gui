﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Collections.Specialized;
using System.Drawing.Printing;

namespace MetalDetectorGUI
{
    
    public partial class mainForm : Form
    {
        const char START_CHAR = '#';        // 0b0010 0011
        const char START_CHAR_STATE = '@';  // 0b0100 0000
        const int DETECTOR_GROUPBOX_OFFSET = 20;
        const int DETECTOR_GROUPBOX_HEIGTH = 52;
        const int DETECTOR_GROUPBOX_WIDTH = 127;
        const int DETECTOR_GROUPBOX_SPACING = 12;

        //
        enum CommunicationSendCommands
        {
            SendTimeOfMeagurment = 0,
            SendLength,
            SendLength_2,
            SendLength_Average,
            SendWidthMin,
            SendWidthMax,
            SendSpeed,
            SendThreshold,
            SendZeroLevel,
            SendMaxLevel,
            SendState,
            SendGlobalSettings,
            SendX_offset,
            SendTuning
        };

        // status of the calculations
        enum CalculationState
        {
            CalculationState_Idle = 0,
            CalculationState_speed,
            CalculationState_length,
            CalculationState_length_2,
            CalculationState_width,
            CalculationState_done
        };


        enum Detectors
        {
            Detector1 = 0,
            Detector2,
            Detector3,
            Detector4,
            Detector5,
            DetectorCount
        };

        // DetectorValues to store
        private uint[] detectorValues = new uint[(int)Detectors.DetectorCount];
        private bool[] detectorIsActive = new bool[(int)Detectors.DetectorCount];
        private uint[] detectorInfo = new uint[(int)Enum.GetNames(typeof(CommunicationSendCommands)).Length];
        private uint[] detectorThreshold = new uint[(int)Detectors.DetectorCount];
        private GroupBox[] detectorGroupBox = new GroupBox[(int)Detectors.DetectorCount];
        private Label[] detectorLabel = new Label[(int)Detectors.DetectorCount];
        private ProgressBar[] detectorProgressBar = new ProgressBar[(int)Detectors.DetectorCount];
        private UInt16[] detectorX_offset = new UInt16[(int)Detectors.DetectorCount];
        private Calibration calibrationForm;
        private Int16 tuningValue = 0;

        public mainForm()
        {
            InitializeComponent();
            getComPorts();
        }

        // get all available com ports
        private void getComPorts()
        {
            string[] ports = SerialPort.GetPortNames();

            // first clear the list
            comPort_box.Items.Clear();

            // add the ports to the list
            foreach (string port in ports)
            {
                comPort_box.Items.Add(port);
            }

            connectComPort_button.Enabled = comPort_box.Items.Count > 0;

            if (comPort_box.Items.Count > 0)
            {
                comPort_box.SelectedIndex = 0;
            }
        }

        // set the com port
        private void comPort_box_SelectedIndexChanged(object sender, EventArgs e)
        {
            comPort_box.Text = comPort_box.GetItemText(comPort_box.SelectedItem);
        }

        // get the com ports
        private void comPort_box_MouseClick(object sender, MouseEventArgs e)
        {
            getComPorts();
        }

        // connect to the com port
        private void connectComPort_button_Click(object sender, EventArgs e)
        {
            if (connectComPort_button.Text == "Connect")
            {
                if (comPort_box.Text != "")
                {
                    serialPort1.PortName = comPort_box.Text;
                    try
                    {
                        serialPort1.Open();
                        connectComPort_button.Text = "Disconnect";
                        comPort_box.Enabled = false;

                        // send data to get the X_offset
                        sendGetX_offset();
                        sendGetTuning();

                    }
                    catch
                    {
                        // create error message with the exception and icon
                        MessageBox.Show("Error opening port " + comPort_box.Text, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                else
                {
                    // create error message with icon
                    MessageBox.Show("Please select a COM port", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                serialPort1.Close();
                connectComPort_button.Text = "Connect";
                comPort_box.Enabled = true;
            }
        }

        // send data to get the X_offset
        private void sendGetX_offset()
        {
            // get x offset
            byte[] dataBuf = BitConverter.GetBytes(0);
            //dataBuf.Reverse();

            byte[] buffer = new byte[1 + 4];
            // add start char
            buffer[0] = (byte)START_CHAR;
            // add command
            buffer[1] = 6;  // sendX_offset
                            // add the data length
            buffer[2] = 1;
            // add detector
            buffer[3] = (byte)Detectors.DetectorCount;
            // add data
            for (int i = 0; i < 1; i++)
            {
                buffer[4 + i] = dataBuf[i];
            }
            serialPort1.Write(buffer, 0, buffer.Length);
            while (serialPort1.BytesToWrite > 0) ;
        }

        // send data to get the X_offset
        private void sendGetTuning()
        {
            // get x offset
            byte[] dataBuf = BitConverter.GetBytes(0);
            //dataBuf.Reverse();

            byte[] buffer = new byte[1 + 4];
            // add start char
            buffer[0] = (byte)START_CHAR;
            // add command
            buffer[1] = 7;  // getTuning
                            // add the data length
            buffer[2] = 1;
            // add detector
            buffer[3] = (byte)Detectors.DetectorCount;
            // add data
            for (int i = 0; i < 1; i++)
            {
                buffer[4 + i] = dataBuf[i];
            }
            serialPort1.Write(buffer, 0, buffer.Length);
            while (serialPort1.BytesToWrite > 0) ;
        }

        // set the form based on connection status
        private void comStatus_timer_Tick(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                comStatus_label.Text = "Connected";
                comStatus_label.ForeColor = Color.Green;
                openCalibration.Enabled = true;
                openHistory_button.Enabled = true;
                showLiveInfo_checkbox.Enabled = true;
            }
            else
            {
                comStatus_label.Text = "Disconnected";
                comStatus_label.ForeColor = Color.Red;
                connectComPort_button.Text = "Connect";
                openCalibration.Enabled = false;
                openHistory_button.Enabled = false;
                showLiveInfo_checkbox.Enabled = false;

                // if calibration form is open, close it
                if (calibrationForm.Visible)
                {
                    calibrationForm.Close();
                }
            }
        }


        // when data is received
        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // read the data from the serial port (when more than 4 bytes are available)
            this.Invoke(new EventHandler(delegate
            {
                while(serialPort1.BytesToRead > 4) readData();
            }));
        }

        // data layout:
        // receving data in format: Start character + Command + data length + data
        // when Start character = START_CHAR_STATE, the data format is: 
        // Start character + detector index + value length + detector state + value 
        private void readData()
        {
            // read until the start character is found and store the data + start character in a buffer
            byte[] tempBuffer = new byte[3];
            while (true)
            {
                while (serialPort1.BytesToRead < 3) ;
                serialPort1.Read(tempBuffer, 0, 1);
                if (tempBuffer[0] == START_CHAR || tempBuffer[0] == START_CHAR_STATE)
                {
                    //tempBuffer[1] = (char)serialPort1.ReadByte
                    serialPort1.Read(tempBuffer, 1, 2);
                    break;
                }
            }

            if (tempBuffer[0] == START_CHAR_STATE)
            {
                while (serialPort1.BytesToRead < 1);
                // read the state of the detector and store it in the array
                detectorIsActive[tempBuffer[1]] = (serialPort1.ReadByte() == 1);
            }


            while (serialPort1.BytesToRead < tempBuffer[2]);
            // create a buffer to store the data
            byte[] buffer = new byte[tempBuffer[2]];
            // read the data from the serial port
            serialPort1.Read(buffer, 0, tempBuffer[2]);

            UInt32 tempValue = 0;
            for (int i = 0; i < tempBuffer[2]; i++)
            {
                tempValue |= (UInt32)(buffer[i] << (8 * i));
            }
            
            if (tempBuffer[0] == START_CHAR)
            {
                CommunicationSendCommands command = (CommunicationSendCommands)tempBuffer[1];


                // create a switchcase with all mebers of CommunicationSendCommands
                switch (command)
                {
                    case CommunicationSendCommands.SendTimeOfMeagurment:
                        if (detectorInfo[(int)CommunicationSendCommands.SendTimeOfMeagurment] != tempValue)
                        {
                            time_label.Text = DateTime.Now.ToString("HH:mm:ss");
                        }
                        break;

                    case CommunicationSendCommands.SendLength:
                        length_1_label.Text = tempValue.ToString();
                        length_1_label.Text += "mm";

                        break;

                    case CommunicationSendCommands.SendLength_2:
                        length_2_label.Text = tempValue.ToString();
                        length_2_label.Text += "mm";
                        break;

                    case CommunicationSendCommands.SendLength_Average:
                        length_average_label.Text = tempValue.ToString();
                        length_average_label.Text += "mm";
                        break;

                    case CommunicationSendCommands.SendWidthMin:
                        widthMin_label.Text = tempValue.ToString();
                        widthMin_label.Text += "mm";
                        break;
                    case CommunicationSendCommands.SendWidthMax:
                        if (detectorInfo[(int)CommunicationSendCommands.SendWidthMin] == tempValue)
                        {
                            widthMax_label.Text = "Unknown";
                        }
                        else
                        {
                            widthMax_label.Text = tempValue.ToString();
                            widthMax_label.Text += "mm";
                        }
                        break;

                    case CommunicationSendCommands.SendSpeed:
                        speed_label.Text = tempValue.ToString();
                        speed_label.Text += "mm/s";
                        break;

                    case CommunicationSendCommands.SendThreshold:
                        // save tempValue in the array and skip the first byte
                        uint threshold = tempValue >> 8;
                        // only save the first byte
                        uint detector = tempValue & 0xFF;
                        // save the threshold in the array
                        detectorThreshold[detector] = threshold;
                        break;

                    case CommunicationSendCommands.SendZeroLevel:
                        break;

                    case CommunicationSendCommands.SendMaxLevel:
                        break;
                        
                    case CommunicationSendCommands.SendState:
                        CalculationState state = (CalculationState)tempValue;
                        switch (state)
                        {
                            case CalculationState.CalculationState_Idle:
                                state_label.Text = "Idle";
                                break;
                                
                            case CalculationState.CalculationState_speed:
                                state_label.Text = "Calc speed";
                                break;

                            case CalculationState.CalculationState_length:
                                state_label.Text = "Calc length";
                                break;

                            case CalculationState.CalculationState_length_2:
                                state_label.Text = "Calc length 2";
                                break;

                            case CalculationState.CalculationState_width:
                                state_label.Text = "Calc width";
                                break;

                            case CalculationState.CalculationState_done:
                                state_label.Text = "Done";
                                
                                break;
                        }

                        break;

                    case CommunicationSendCommands.SendGlobalSettings:
                        break;

                    case CommunicationSendCommands.SendX_offset:

                        // save tempValue in the array and skip the first byte
                        uint xOffset = tempValue >> 8;
                        // only save the first byte
                        uint detectorNum = tempValue & 0xFF;
                        // save the threshold in the array
                        detectorX_offset[detectorNum] = (UInt16)xOffset;
                        break;

                    case CommunicationSendCommands.SendTuning:
                        tuningValue = (Int16)(UInt16)tempValue;
                        break;
                }

                // store the value in the array
                detectorInfo[(int)command] = tempValue;
            }
            else if (tempBuffer[0] == START_CHAR_STATE)
            {
                detectorValues[tempBuffer[1]] = tempValue;
                try
                {
                    detectorProgressBar[tempBuffer[1]].Value = (int)tempValue;
                    detectorLabel[tempBuffer[1]].ForeColor = detectorIsActive[tempBuffer[1]] ? Color.Green : Color.Red;
                }
                catch
                {
                    ;
                }
            }


            if (detectorInfo[(int)CommunicationSendCommands.SendSpeed] > 0
                && detectorInfo[(int)CommunicationSendCommands.SendTimeOfMeagurment] > 0
                && detectorInfo[(int)CommunicationSendCommands.SendLength_Average] > 0)
            {
                updateHistoryBox();
                // clear the array detectorInfo
                for(uint i = 0; i < detectorInfo.Length; i++)
                {
                    detectorInfo[i] = 0;
                }
            }
        }

        // update the history box
        private void updateHistoryBox()
        {
            // create a string to store the data
            string text = "";
            text += "Time: " + DateTime.Now.ToString("HH:mm:ss");
            text += "\tSpeed: " + detectorInfo[(int)CommunicationSendCommands.SendSpeed] + " mm/s";
            text += "\tLength: " + detectorInfo[(int)CommunicationSendCommands.SendLength_Average] + " mm";
            text += "\tWidth minimal: " + detectorInfo[(int)CommunicationSendCommands.SendWidthMin] + " mm\t";
            // check if the max width is unknown
            if (detectorInfo[(int)CommunicationSendCommands.SendWidthMin] == detectorInfo[(int)CommunicationSendCommands.SendWidthMax])
            {
                text += "\tWidth maximal: unkown";
            }
            else
            {
                text += "\tWidth maximal: " + detectorInfo[(int)CommunicationSendCommands.SendWidthMax] + " mm";  
            }
            
            // add the string to the historyListBox
            History history = new History();
            history.AddToHistory(text);
        }

        // open the calibration form and disable this form
        private void openCalibration_Click(object sender, EventArgs e)
        {
            // show the Calibration form
            this.Enabled = false;
            calibrationForm = new Calibration(serialPort1, detectorValues);
            calibrationForm.Show();
        }

        // open the history form 
        private void openHistory_button_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<History>().Any())
            {
                History historyForm = new History();
                historyForm.Show();
            }
        }

        // show progress bar when te firt detector is active and hide it when the last detector is active
        private void statusTimer_Tick(object sender, EventArgs e)
        {
            if (detectorIsActive[(int)Detectors.Detector1]) {
                progressBar1.Visible = true;
            }
            else if (detectorIsActive[(int)Detectors.Detector2] || !serialPort1.IsOpen)
            {
                progressBar1.Visible = false;
            }
        }

        // on load
        private void Form1_Load(object sender, EventArgs e)
        {
            // update the position of the object box
            updateThePositionOfTheObject_groupbox();
            
            calibrationForm = new Calibration(serialPort1, detectorValues);
            
            // resize the detectorInfo panel
            // get the detectorPanel offset from the form bottom
            int detectorPanelY_offset = this.Height - detectorInfo_panel.Height;
            detectorInfo_panel.Height = DETECTOR_GROUPBOX_HEIGTH * (int)Detectors.DetectorCount + DETECTOR_GROUPBOX_OFFSET;

            // create the status groupBoxes of the detectors in the detectorInfo_panel
            // for loop of the enum Detectors
            foreach (Detectors detector in Enum.GetValues(typeof(Detectors)))
            {
                uint i = (uint)detector;
                if (detector == Detectors.DetectorCount) break;
                // create a new groupBox
                detectorGroupBox[i] = new GroupBox();
                detectorGroupBox[i].Name = detector.ToString();
                detectorGroupBox[i].Size = new System.Drawing.Size(DETECTOR_GROUPBOX_WIDTH, DETECTOR_GROUPBOX_HEIGTH);
                detectorGroupBox[i].Location = new System.Drawing.Point(15, DETECTOR_GROUPBOX_SPACING + (int)(detectorGroupBox[i].Height * i));
                detectorGroupBox[i].TabIndex = 24;
                detectorGroupBox[i].TabStop = false;
                detectorGroupBox[i].Text = "Detector " + i.ToString();
                //detectorGroupBox[i].Visible = false;
                detectorInfo_panel.Controls.Add(detectorGroupBox[i]);

                // create a new label
                detectorLabel[i] = new Label();
                detectorLabel[i].AutoSize = true;
                detectorLabel[i].BackColor = System.Drawing.Color.Transparent;
                detectorLabel[i].Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                detectorLabel[i].ForeColor = System.Drawing.Color.Red;
                detectorLabel[i].Location = new System.Drawing.Point(6, 16);
                detectorLabel[i].Name = "label_" + detector.ToString();
                detectorLabel[i].Size = new System.Drawing.Size(25, 25);
                detectorLabel[i].TabIndex = 0;
                detectorLabel[i].Text = "●";
                detectorGroupBox[i].Controls.Add(detectorLabel[i]);

                // create a new progressBar
                detectorProgressBar[i] = new ProgressBar();
                detectorProgressBar[i].Location = new System.Drawing.Point(37, 26);
                detectorProgressBar[i].Name = "progressBar_" + detector.ToString();
                detectorProgressBar[i].Size = new System.Drawing.Size(76, 11);
                detectorProgressBar[i].TabIndex = 1;
                detectorProgressBar[i].Maximum = 1023;
                detectorGroupBox[i].Controls.Add(detectorProgressBar[i]);

            }
            
            // adjust the form size
            this.MinimumSize = new System.Drawing.Size(this.MinimumSize.Width, detectorPanelY_offset + detectorInfo_panel.Height);
        }

        // returns the detector thresholds array
        public uint[] getDetectorThresholds()
        {
            return detectorThreshold;
        }

        // returns the detector Xoffset array
        public UInt16[] getDetectorX_offset()
        {
            return detectorX_offset;
        }

        // show the detector info (panel)
        private void showLiveInfo_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            detectorInfo_panel.Visible = showLiveInfo_checkbox.Checked;
            updateThePositionOfTheObject_groupbox();
        }

        // adjust the position of the object groupbox
        // set to center of available space
        private void updateThePositionOfTheObject_groupbox()
        {
            int availableSpace = showLiveInfo_checkbox.Checked ? detectorInfo_panel.Location.X : this.Width;
            object_groupBox.Location = new Point((availableSpace - object_groupBox.Width) / 2, object_groupBox.Location.Y);
        }

        public Int16 getTuningValue()
        {
            return tuningValue;
        }

        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort1.Close();
        }
    }
}
