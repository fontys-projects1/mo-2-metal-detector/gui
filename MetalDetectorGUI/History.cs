﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetalDetectorGUI
{
    partial class History : Form
    {
        private static string[] history = new string[0];

        // constructor
        public History()
        {
            InitializeComponent();
        }
        // create a function to add string to the listbox
        public void AddToHistory(string str)
        {
            // create a new array with one more element
            string[] newHistory = new string[history.Length + 1];
            // copy the old array to the new array
            history.CopyTo(newHistory, 0);
            // add the new string to the new array
            newHistory[newHistory.Length - 1] = str;
            // assign the new array to the old array
            history = newHistory;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // add the reversed history to the listbox, only if the data is new
            if (history_listBox.Items.Count != history.Length)
            {
                // add the history to the listbox
                history_listBox.Items.Clear();
                for (int i = history.Length - 1; i >= 0; i--)
                {
                    history_listBox.Items.Add(history[i]);
                }
            }

        }

        private void History_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void History_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Stop();
        }
    }
}
